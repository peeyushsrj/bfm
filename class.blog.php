<?php
class blog{
	private $host;
	private $user;
	private $pwd;
	private $db;
	private $dbms;
	private $table;
	function __construct($con)
	{
		$this->host = $con[0];
		$this->user = $con[1];
		$this->pwd = $con[2];
		$this->db = $con[3];
		$this->table = $con[4];
		//Connection variales set
		$this->dbms = new mysqli($this->host, $this->user, $this->pwd, $this->db);
		//Connection Stage Completed
		echo $this->checkConError();
	}
	function checkConError(){
		if($this->dbms->connect_errno > 0){
			return $this->dbms->connect_error;
		}
		if (!isset($this->table)) {
			return "Table not defined";
		}
		$sql = "SHOW TABLES";
		$result=$this->dbms->query($sql);
		while ($row=$result->fetch_assoc()) {
			if ($row["Tables_in_".$this->db]!=$this->table) {
				$found=0;
			}
			else{
				$found=1; break;
			}
		}
		if ($found==0) {
			return $this->table." not found in the given database ".$this->db;
		}
		//Return if error 
	}
	function read($info){
		foreach ($info as $key => $value) {
			$reado=$reado.$key."='".$value."', ";
		}
		$reado = rtrim($reado, ", ");
		$sql="SELECT * FROM ".$this->table." WHERE ".$reado;
		$result=$this->dbms->query($sql);
		if ($this->dbms->affected_rows==0) {
			return "No Results Found!";
		}
		else{
			$x=[];
			while ($row = $result->fetch_assoc()) {
				array_push($x, $row);
			}
			return $x;
		}
		/*if (!$result) {
			return $this->dbms->error;
		}No Need because this whole thing is abstractive*/
	}
	function delete($info){
		foreach ($info as $key => $value) {
			$reado=$reado.$key."='".$value."', ";
		}
		$reado = rtrim($reado, ", ");
		$sql="DELETE FROM ".$this->table." WHERE ".$reado;
		$result=$this->dbms->query($sql);
		if ($this->dbms->affected_rows>0) {
			return "Successfully Deleted";
		}
	}
	function create($info){
		foreach ($info as $key => $value) {
			$r=$r.$key.", ";
			$s=$s.'"'.$value.'", ';
		}
		$r = rtrim($r, ", ");
		$s = rtrim($s, ", ");
		$sql="INSERT INTO ".$this->table."(".$r.") VALUES(".$s.")";
		$result=$this->dbms->query($sql);
		if ($this->dbms->affected_rows>0) {
			return "Successfully Inserted";
		}
	}
	function update($info, $info2){
		foreach ($info as $key => $value) {
			$reado=$reado.$key."='".$value."', ";
		}
		$reado = rtrim($reado, ", ");
		foreach ($info2 as $key => $value) {
			$reado2=$reado2.$key."='".$value."', ";
		}
		$reado2 = rtrim($reado2, ", ");
		$sql="UPDATE ".$this->table." SET ".$reado." WHERE ".$reado2;
		$result=$this->dbms->query($sql);
		if ($this->dbms->affected_rows>0) {
			return "Successfully Updated";
		}
	}
	function __destroy(){
		echo "Alonzy";
	}
	//Error Checking Left on testcases
	//CRUD simple app created
}
?>